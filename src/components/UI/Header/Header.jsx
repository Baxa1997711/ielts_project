import React, { useState, useEffect } from 'react';
import styles from './header.module.scss'
import { Routes, Route, Link } from 'react-router-dom'
import { Container, Drawer, Button } from '@mui/material'
import {
    MenuBurgerIcon,
    InstagramIcon,
    FacebookIcon,
    TelegramIcon,
    YoutubeIcon

} from '../../../images/icons'

function Header(props) {
    const[openDrawer, setOpenDrawer] = useState(false)
    
    const handleClose = () => {
        setOpenDrawer(false)
    }
    
    return (
        <div className={styles.header}>
            <Container>
                <div className={styles.header_content}>
                <div className={styles.logo}>
                    <Button onClick={() => setOpenDrawer(true)} className={styles.menu_btn}>
                        <MenuBurgerIcon/>
                    </Button>
                    <Link to="/" style={{textDecoration: 'none'}}>
                        <h3 className={styles.header_logo}>IELTS<span>BAND</span></h3>
                    </Link>
                </div>
                
                <div className={styles.header_navigation}>
                <ul className={styles.navbar}>
                    <li className={styles.navbar_item}>
                        <Link to='' style={{textDecoration: 'none'}}>
                            <p>About us</p>
                        </Link>
                    </li>    
                    <li className={styles.navbar_item}>
                        <Link to='' style={{textDecoration: 'none'}}>
                            <p>contact</p>
                        </Link>
                    </li>    
                    <li className={styles.navbar_item}>
                        <Link to='' style={{textDecoration: 'none'}}>
                            <p>Our goals</p>
                        </Link>
                    </li>    
                </ul> 
                <div className={styles.social}>
                
                    <a href="#" className={styles.social_links}>
                        <InstagramIcon/>
                    </a>
                    <a href="#" className={styles.social_links}>
                        <TelegramIcon/>
                    </a>
                    <a href="#" className={styles.social_links}>
                        <FacebookIcon/>
                    </a>
                    <a href="#" className={styles.social_links}>
                        <YoutubeIcon/>
                    </a>
                </div>    
                </div>   
                </div>
            </Container>
            
            <Drawer
            open={openDrawer}
            anchor='left'
            onClose={handleClose}
            >
              <div className={styles.menu_drawer}>
                <div className={styles.menu_drawer_content}>
                    Menu
                </div>
              </div>  
            </Drawer>
        </div>
    );
}

export default Header;