import { Container } from '@mui/system';
import { Button } from '@mui/material'
import React from 'react';
import styles from './infoielts.module.scss'
import InfoImage from '../../../images/image/info_test.png'

function Infoielts(props) {
    return (
        <div className={styles.info}>
           <Container>
                <div className={styles.info_content}>
                    <h1 className={styles.info_title}>What is IELTS?</h1>
                    <p className={styles.info_subtitle}>Expanding outlook</p>
                    <div className={styles.info_content_item}>
                        <div className={styles.info_content_context}>
                            <h2>What is IELTS Test?</h2>
                            <p className={styles.info_content_subtitle}><span>Lorem, ipsum dolor sit</span> amet consectetur adipisicing elit. Debitis natus commodi quasi?</p>
                            <p className={styles.info_content_sub}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi, alias?
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae sit adipisci iure commodi aspernatur fugit rem nostrum, quia id ducimus eaque quis delectus eligendi inventore repellendus consequatur, dignissimos ut ex qui cumque consequuntur repellat nisi perferendis sapiente. Ratione, nulla provident.</p>
                      <Button className={styles.moreInfo_btn}>More Info</Button>
                        </div>
                        <div className={styles.info_content_image}>
                            <img src={InfoImage} alt="" />
                        </div>
                    </div>
                </div>
           </Container>
        </div>
    );
}

export default Infoielts;