import { Container } from '@mui/system';
import React from 'react';
import styles from './service.module.scss'

function Service(props) {
    return (
        <div className={styles.service}>
            <Container>
                <div className={styles.service_content}>
                    <h1 className={styles.service_title}>What We offer?</h1>
                    <p className={styles.service_subtitle}>Access to materials</p>
                    
                    <div className={styles.service_context}>
                        <p className={styles.service_info}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum nobis dignissimos atque, iste dolore veritatis sequi eligendi fugit! Tempore ipsam praesentium ipsum odit, non possimus corporis harum? Eum, doloremque dignissimos.</p>
                    </div>
                </div>
            </Container>
        </div>
    );
}

export default Service;