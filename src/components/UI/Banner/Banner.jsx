import React, { useRef } from 'react';
import styles from './banner.module.scss';
import { Container, Button } from '@mui/material'
import BannerImg from '../../../images/image/banner_test2.webp'
import { ExtraDecorIcon } from '../../../images/icons.js'

const textAnimation = {
  hidden: {
    x: -100,
    opacity: 0
  },
  visible: custom =>({
    x: 0,
    opacity: 1,
    transition: { delay: custom * 0.2 },
  })
}


function Banner( { data_banner } ) {
    const settings = {
        fade: true,
        infinite: true,
        speed: 800,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false, 
    }
    const slider = useRef();
    
    const next = () => {
      slider.current.slickNext();
    }
    const previous = () => {
      slider.current.slickPrev();
    }
    return (
       <>
        <div  className={styles.banner}>
            {/* <img
              src={BannerImg}
              priority={true}
              alt='cspace'
              unoptimized={true}
              className={styles.banner_img}
              layout='fill'
            /> */}
            <div className={styles.extra_icon1}>
                  <ExtraDecorIcon/>
              </div>
            <div className={styles.overlay}>
              
            </div>
            
        </div>
        <Container>
                <div className={styles.banner_content}>
                    <div className={styles.banner_context}>
                      <p className={styles.promo}>English Consulting Website</p>
                        <h1>IELTS Preparation Website</h1>
                        <p className={styles.write_goal}>
                        We help our users succeed in IELTS Writing by creating writing samples,
                        giving feedbacks and publishing materials.
                        </p>
                        <div className={styles.btns}>
                        <Button className={styles.send_btn}>Send writing...</Button>
                        <Button className={styles.see_btn}>More samples...</Button>
                        </div>
                    </div>
                </div>
        </Container>
       </>
    );
}

export default Banner;



