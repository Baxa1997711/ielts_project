import React from 'react';
import styles from './about.module.scss'
import { Container, Button } from '@mui/material'
import aboutImage from '../../../images/image/test_english.jpg'

function About(props) {
    return (
        <div className={styles.about}>
          <Container>
            <div className={styles.about_content}>
                <h1 className={styles.about_title}>About Website</h1> 
                <p className={styles.about_subtitle}>Exploring more Ideas</p> 
                
                <div className={styles.about_content_item}>
                  <div className={styles.about_content_image}>
                    <img src={aboutImage} alt="" />
                  </div>
                <div className={styles.about_info}>
                  <h3>The Website is for Free Preparation for IELTS Test</h3>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur mollitia quia dolorum alias voluptate quod praesentium minus non velit minima accusantium asperiores, explicabo nihil, facilis ipsa in! Animi soluta dignissimos cupiditate fugiat reiciendis?
                  <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam molestias quaerat eligendi voluptates! Voluptatum, tempora accusantium tempore, voluptas cupiditate obcaecati quia sit mollitia nam inventore a architecto id deserunt nemo.</span></p>  
                  <Button className={styles.info_btn}>More Info</Button>
                </div> 
                </div>
                {/* <div className={styles.about_info_context}>
                    
                </div> */}
             </div>  
          </Container>  
        </div>
    );
}

export default About;