import React from 'react';
import About from '../../UI/About/About';
import Banner from '../../UI/Banner/Banner';
import Infoielts from '../../UI/Infoielts/Infoielts';
import Service from '../../UI/Service/Service';

function Main(props) {
    return (
        <div>
           <Banner/> 
           <About/>
           <Infoielts/>
           <Service/>
        </div>
    );
}

export default Main;