import Header from "./components/UI/Header/Header";
import Main from "./components/pages/Main/Main";
import { Routes, Route, Link } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes>
      <Route exact path="/" element={<Main/>}/>
      </Routes>
     
    </div>
  );
}

export default App;
